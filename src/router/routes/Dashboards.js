import { lazy } from 'react'

const DashboardRoutes = [
  // Dashboards
  {
    path: '/dashboard/analytics',
    component: lazy(() => import('../../views/dashboard/analytics'))
  },
  {
    path: '/dashboard/admin',
    component: lazy(() => import('../../views/dashboard/admin')),
    exact: true
  }
]

export default DashboardRoutes
