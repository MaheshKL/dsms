import { Mail, MessageSquare, CheckSquare, Calendar, FileText, Circle, ShoppingCart, User } from 'react-feather'

export default [
  {
    header: 'Super Admin Dashboard'
  },
  {
    id: 'dashboard',
    title: 'DashBoard',
     icon: <Circle size={20} />,
     navLink: '/dashboard/admin'
  },
  {
    id: 'user',
    title: 'Users',
    icon: <User size={20} />,
   
    children:[
      {
        id: 'role',
        title: 'Roles',
        icon: <Circle size={12} />,
        navLink: '/apps/invoice/list'
      },
      {
        id: 'user',
        title: 'Users',
        icon: <Circle size={12} />,
        navLink: '/apps/users'
      }
    ]
  },
  {
    id: 'setting',
    title: 'Settings',
     icon: < Circle size={20} />,
    navLink: '/apps/todo',
    children:[
      {
        id: 'shop1',
        title: 'Admin Panel Settings',
        icon: <Circle size={12} />,
        navLink: '/apps/ecommerce/user'
      },
      {
        id: 'shop1',
        title: 'School level Settings',
        icon: <Circle size={12} />,
        navLink: '/apps/ecommerce/user'
      },
      {
        id: 'shop1',
        title: 'Student level Settings',
        icon: <Circle size={12} />,
        navLink: '/apps/ecommerce/user'
      },
      {
        id: 'shop1',
        title: 'Integration configuration Settings',
        icon: <Circle size={12} />,
        navLink: '/apps/ecommerce/user'
      },
      {
        id: 'shop1',
        title: 'Other thirdparty configuration Settings',
        icon: <Circle size={12} />,
        navLink: '/apps/ecommerce/user'
      }
    ]
  },
  
  {
    id: 'calendar',
    title: 'Calendar',
    icon: <Calendar size={20} />,
    navLink: '/apps/calendar'
  },
  {
    id: 'invoiceApp',
    title: 'CMS',
    icon: <FileText size={20} />,
    navLink: '/apps/cms'
  },

  {
        id: 'schools',
        title: 'Schools',
        icon: <Circle size={12} />,
         navLink: '/apps/school',
        children:[
          {
            id: 'shop1',
            title: 'Schools',
            icon: <Circle size={12} />,
            navLink: '/apps/schools'
            
          },
          {
            id: 'shop1',
            title: 'User School',
            icon: <Circle size={12} />,
            navLink: '/apps/userschools'
          },
          {
            id: 'shop1',
            title: 'School CMS Control',
            icon: <Circle size={12} />,
            navLink: '/apps/schoolscontrol'
          }
        ]
      },
      {
        id: 'profile',
        title: 'Profile',
        icon: <Calendar size={20} />,
        navLink: '/apps/profile'
      },

      {
        id: 'services',
        title: 'Services',
         icon: < Circle size={20} />,
         navLink: '/apps/services',
        children:[
          {
            id: 'shop1',
            title: 'Main Categorization',
            icon: <Circle size={12} />,
            navLink: '/apps/maincategorization'
          },
          {
            id: 'shop1',
            title: 'Main Categorization',
            icon: <Circle size={12} />
            // navLink: '/apps/user/view'
          }
         
        ]
      },
      {
        id: 'enduser',
        title: 'End Users',
         icon: < Circle size={20} />,
        // navLink: '/apps/todo',
        children:[
          {
            id: 'shop1',
            title: 'Control over End User',
            icon: <Circle size={12} />,
            navLink: '/apps/user/list'
          }
          
        ]
      },
      {
        id: 'teachers',
        title: 'Teachers',
         icon: < Circle size={20} />,
        // navLink: '/apps/todo',
        children:[
          {
            id: 'shop1',
            title: 'Control over User',
            icon: <Circle size={12} />
            // navLink: '/apps/user/list'
          }
        ]
      },
      {
        id: 'report',
        title: 'Reports',
         icon: < Circle size={20} />,
        navLink: '/apps/report'
      },
      // {
      //   id: 'chat',
      //   title: 'Monitization',
      //    icon: < Circle size={20} />
      //   // navLink: '/apps/todo',
      // },
      {
        id: 'chat',
        title: 'Admin to School Level access',
         icon: < Circle size={20} />
        // navLink: '/apps/todo',
      },
        {
          id: 'users',
          title: 'Front end Functionality',
          icon: <User size={20} />,
          children: [
            {
              id: 'list',
              title: 'Social Media News feeds',
              icon: <Circle size={12} />
              // navLink: '/apps/user/list'
            },
            {
              id: 'view',
              title: 'Listing of schools ',
              icon: <Circle size={12} />
              // navLink: '/apps/user/view'
            },
            {
              id: 'edit',
              title: 'Login in school level',
              icon: <Circle size={12} />
              // navLink: '/apps/user/edit'
            }
            
          ]
        }

]
